import React, {useState} from 'react'
import { StyleSheet, Text, View, Button}from 'react-native'
import { Input } from 'react-native-elements';
import {Picker} from '@react-native-picker/picker';

const registerBoss = (props) => {

    const [text, onChangeText] = React.useState("Useless Text");
    const [number, onChangeNumber] = React.useState(null);

    //Inicializando Variaveis
    const [selectedValue, setSelectedValue] = useState();
    const [name, setName] = useState(null)
    const [phone, setPhone] = useState(null)
    const [email, setEmail] = useState(null)
    const [password, setPassword] = useState(null)

    //Variaveis de erros
    const [errorName, setErrorName] = useState(null)
    const [errorPhone, setErrorPhone] = useState(null)
    const [errorEmail, setErrorEmail] = useState(null)
    const [errorPassword, setErrorPassword] = useState(null)

    const validateFiields = () => {

        let error = false
        setErrorName(null)
        setErrorPhone(null)
        setErrorEmail(null)
        setErrorPassword(null)

        console.log(name)
        if(name == null){
            setErrorName("Informe Seu Nome")
            error = true
        }
        if(phone == null){
            setErrorPhone("Digite Seu Telefone")
            error = true
        }
        if(email == null){
            setErrorEmail("Informe Seu Email")
            error = true
        }
        if(password == null){
            setErrorPassword("Informe Uma Senha")
            error = true
        }

        return !error
        
    }

    const toSave = () =>{
        if(validateFiields()){
            console.log(validateFiields)
        }
    }

        return(
            <View style={styles.container}>
                <Text style={styles.text}>Cadastrar dados Pessoais</Text>
            
                <View style={styles.form}>
                    <Input
                        style={styles.input} 
                        onChangeText={value => {
                            setName(value)
                            setErrorName(null)
                        }}
                        placeholder="Informe Seu Nome"
                        keyboardType="default"
                        errorMessage={errorName}/>
                    <Input
                        style={styles.input} 
                        onChangeText={value => {
                            setPhone(value)
                            setErrorPhone(null)
                        }}
                        placeholder="Informe Seu Telefone"
                        keyboardType="phone-pad"
                        errorMessage={errorPhone}/>
                    <Input
                        style={styles.input} 
                        onChangeText={value => {
                            setEmail(value)
                            setErrorEmail(null)
                        }}
                        placeholder="Informe Seu Email"
                        keyboardType="email-address"
                        errorMessage={errorEmail}/>
                    <Input
                        style={styles.input} 
                        onChangeText={value => {
                            setPassword(value)
                            setErrorPassword(null)
                        }}
                        placeholder="Informe Sua Senha"
                        keyboardType="visible-password"
                        errorMessage={errorPassword}/> 
                    </View>    

                    <View style={styles.gender}>
                        <Picker
                            selectedValue={selectedValue}
                            onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                        >
                            <Picker.Item label="Sexo" enabled={false}/>
                            <Picker.Item label="Masculino" value="Masculino" />
                            <Picker.Item label="Feminino" value="Feminino" />
                        </Picker>
                    </View>   
                    <View style={styles.button}>
                        <Button
                            onPress={() => props.navigation.navigate("Cadastrar Empresa")}
                            title='Avançar'/>
                      </View>
            </View>
        )
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        alignItems: 'center',
        padding: 50
    },
    text: {
        fontSize: 23,
        marginTop:20,
    },
    form: {
        marginTop: 25,
        width: 350
    },
    input: {
        marginTop: 15,
        height: 50,
      },
    gender:{
        marginTop: 15,
        width: 350,
        height: 35,
        borderWidth: 1,
        borderRadius: 8,
        color: 'black',
    },
    button: {
        flex: 1,
        marginTop: 15,
        height: 50,
        width: 150,
    },
})

export default registerBoss;