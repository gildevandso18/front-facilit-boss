import React, {useState} from 'react'
import { StyleSheet, Text, View, Button}from 'react-native'
import { Input } from 'react-native-elements';


const registerAdress = (props) => {

    const [text, onChangeText] = React.useState("Useless Text");

    const [number, onChangeNumber] = React.useState(null);
    const [selectedValue, setSelectedValue] = useState();

    //Declarando Variaveis
    const[state, setState] = useState(null)
    const[city, setCity] = useState(null)
    const[district, setDistrict] = useState(null)
    const[houseNumber, setHouseNumber] = useState(null)
    const[cep, setCep] = useState(null)

    //Variaveis de erro
    const[errorState, setErrorState] = useState(null)
    const[errorCity, setErrorCity] = useState(null)
    const[errorDistrict, setErrorDistrict] = useState(null)
    const[errorHouseNumber, setErrorHouseNumber] = useState(null)
    const[errorCep, setErrorCep] = useState(null)

    const validateAddress = () => {

        let error = false
        setErrorState(null)
        setErrorCity(null)
        setErrorDistrict(null)
        setErrorHouseNumber(null)
        setErrorHouseNumber(null)

        if(state == null){
            setErrorState("Informe Seu Estado")
            error = true
        }
        if(city == null){
            setErrorCity("Informe Sua Cidade")
            error = true
        }
        if(district == null){
            setErrorDistrict("Informe O Seu Bairro")
            error = true
        }
        if(houseNumber == null){
            setErrorHouseNumber("Informe N° Casa")  
            error = true
        }
        if(cep == null) {
            setErrorCep("Informe O CEP")
            error = true
        }

        return !error

    }
    const toSaveAddress = () => {
        if(validateAddress()){
            console.log(validateAddress)
        }
    }
    
        return(
            <View style={styles.container}>
                <Text style={styles.text}>Cadastrar dados Empresa</Text>
            
                <View style={styles.form}>
                    <Input
                        style={styles.input} 
                        onChangeText={value => {
                            setState(value)
                            setErrorState(null)
                        }}
                        placeholder="Informe Seu Estado"
                        keyboardType="default"
                        errorMessage={errorState}/>
                    <Input
                        style={styles.input} 
                        onChangeText={value => {
                            setCity(value)
                            setErrorCity(null)
                        }}
                        placeholder="Informe Seu Cidade"
                        keyboardType="default"
                        errorMessage={errorCity}/>
                    <Input
                        style={styles.input} 
                        onChangeText={value => {
                            setDistrict(value)
                            setErrorDistrict(null)
                        }}
                        placeholder="Informe Seu Bairro"
                        keyboardType="default"
                        errorMessage={errorDistrict}/>
                    <Input
                        style={styles.input} 
                        onChangeText={value => {
                            setHouseNumber(value)
                            setErrorHouseNumber(null)
                        }}
                        placeholder="Informe Numero da Casa"
                        keyboardType="number-pad"
                        errorMessage={errorHouseNumber}/> 
                    <Input
                        //Deveriamos usar uma Api de CEP e colocar
                        //pra prencher todos os Dados Acima
                        style={styles.input} 
                        onChangeText={value => {
                            setCep(value)
                            setErrorCep(null)
                        }}
                        placeholder="Informe o CEP" 
                        keyboardType="number-pad"
                        errorMessage={errorCep}/> 
                    </View>    
                    <View style={styles.button}>
                        <Button
                             onPress={() => props.navigation.navigate("Definir Horarios")}
                            title='Avançar'/>
                      </View>
            </View>
        )
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        alignItems: 'center',
        padding: 50
    },
    text: {
        fontSize: 23,
        marginTop:20,
    },
    form: {
        marginTop: 25,
        width: 350
    },
    input: {
        marginTop: 15,
        height: 50,
      },
    button: {
        flex: 1,
        width: 150,
        marginTop: 15,
        height: 50,
    },
})

export default registerAdress;