import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput, Button, Alert } from 'react-native'
import { Picker } from '@react-native-picker/picker'
import { CheckBox } from 'react-native-elements/dist/checkbox/CheckBox'
import Api from '../controllers/api'

const openingHours = (props) => {

    const [selectedOpenMorning, setSelectedOpenMorning] = useState()
    const [selectedCloseMorning, setSelectedCloseMorning] = useState()

    const [selectedOpenAfternoon, setSelectedOpenAfternoon] = useState()
    const [selectedCloseAfternoon, setSelectedCloseAfternoon] = useState()

    const [lunch, setLunch] = useState(false)

    const calculateHours = () => {

        console.log("Abrir " + selectedOpenMorning, " Fechar " + selectedCloseMorning,
            " Abrir " + selectedOpenAfternoon, " Fechar " + selectedCloseAfternoon)

        let data = new FormData()

        data.append("workerId", "1")
        data.append("shopId", "1")
        data.append("date", "25/08/2021")

        for (let timeInMinutes = 0; timeInMinutes < 1440; timeInMinutes += 10) {

            let hours = Math.trunc(timeInMinutes / 60)

            if (hours < 10) {
                hours = "0" + hours
            }

            let minuntes = timeInMinutes % 60

            if (minuntes < 10) {
                minuntes = "0" + minuntes
            }

            let time = "t" + hours + "h" + minuntes

            if (selectedOpenMorning >= selectedCloseMorning) {
                Alert.alert("Horarios  Invalidos", "abrir maior que fechar ")
                return
            }
            if (selectedOpenAfternoon >= selectedCloseAfternoon) {
                Alert.alert("Horarios  Invalidos", "abrir maior que fechar2 ")
                return
            }

            if ((timeInMinutes >= selectedOpenMorning && timeInMinutes < selectedCloseMorning)
                || (timeInMinutes >= selectedOpenAfternoon && timeInMinutes < selectedCloseAfternoon)) {
                data.append(time, 0)
            } else {
                data.append(time, 1)
            }

        }
        Api.post('times/create', data).then( response =>
            console.log(response.data)            
        ).catch( err =>
            console.log(err.request.response)
        );
        
        {/*...hours.map((item, index) => {return (< Picker.Item label={item} value={index} />);})*/}
        //load api : times/create ( workerId , date ) 
        //console.log(data)
    }
    const hours = [
        label = "08:00", value = "480",
        label = "08:30", value = "510",
        label = "09:00", value = "540",
        label = "09:30", value = "570",
        label = "10:00", value = "600",
        label = "10:30", value = "630",
        label = "11:00", value = "660",
        label = "11:30", value = "790",
    ]

    return (
        <View style={styles.container}>
            <Text style={styles.text}>Horario de Funcionamento</Text>
            <View style={styles.picker}>
                <Picker
                    selectedValue={selectedOpenMorning}
                    style={{ height: 30, width: 150 }}
                    onValueChange={(itemValue, itemIndex) => setSelectedOpenMorning(itemValue)}
                >
                    <Picker.Item label="Abrir" enabled={false} />
                    <Picker.Item label="08:00" value="480" />
                    <Picker.Item label="08:30" value="510" />
                    <Picker.Item label="09:00" value="540" />
                    <Picker.Item label="09:30" value="570" />
                    <Picker.Item label="10:00" value="600" />
                    <Picker.Item label="10:30" value="630" />
                    <Picker.Item label="11:00" value="660" />
                    <Picker.Item label="11:30" value="690" />
                    <Picker.Item label="12:00" value="720" />
                </Picker>
            </View>
            <View style={styles.picker} >
                <Picker
                    selectedValue={selectedCloseMorning}
                    style={{ height: 30, width: 150 }}
                    onValueChange={(itemValue, itemIndex) => setSelectedCloseMorning(itemValue)}
                >
                    <Picker.Item label="Fecha" enabled={false} />
                    <Picker.Item label="08:00" value="480" />
                    <Picker.Item label="08:30" value="510" />
                    <Picker.Item label="09:00" value="540" />
                    <Picker.Item label="09:30" value="570" />
                    <Picker.Item label="10:00" value="600" />
                    <Picker.Item label="10:30" value="630" />
                    <Picker.Item label="11:00" value="660" />
                    <Picker.Item label="11:30" value="690" />
                    <Picker.Item label="12:00" value="720" />
                </Picker>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <CheckBox
                    center
                    iconType="material"
                    checkedIcon="check-circle"
                    uncheckedIcon="check-circle"
                    checkedColor="green"
                    checked={lunch}
                    onPress={() => setLunch(!lunch)} />
                <Text style={styles.text}>Tem Horário de Almoço</Text>
            </View>
            {lunch &&
                <View>
                    <View style={styles.picker}>
                        <Picker
                            selectedValue={selectedOpenAfternoon}
                            style={{ height: 30, width: 150 }}
                            onValueChange={(itemValue, itemIndex) => setSelectedOpenAfternoon(itemValue)}
                        >
                            <Picker.Item label="Abir" enabled={false} />
                            <Picker.Item label="14:00" value="840" />
                            <Picker.Item label="14:30" value="870" />
                            <Picker.Item label="15:00" value="900" />
                            <Picker.Item label="15:30" value="930" />
                            <Picker.Item label="16:00" value="960" />
                            <Picker.Item label="16:30" value="990" />
                            <Picker.Item label="17:00" value="1020" />
                        </Picker>
                    </View>
                    <View style={styles.picker}>
                        <Picker
                            selectedValue={selectedCloseAfternoon}
                            style={{ height: 30, width: 150 }}
                            onValueChange={(itemValue, itemIndex) => setSelectedCloseAfternoon(itemValue)}
                        >
                            <Picker.Item label="Tarde" enabled={false} />
                            <Picker.Item label="14:00" value="840" />
                            <Picker.Item label="14:30" value="870" />
                            <Picker.Item label="15:00" value="900" />
                            <Picker.Item label="15:30" value="930" />
                            <Picker.Item label="16:00" value="960" />
                            <Picker.Item label="16:30" value="990" />

                        </Picker>
                    </View>
                </View>
            }
            <View style={styles.Button}>
                <Button
                    title={'Selecionar'}
                    onPress={() => calculateHours()} />
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 50
    },
    text: {
        fontSize: 20,
    },
    picker: {
        marginTop: 15,
        paddingTop: 40,
        width: 150,
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 8,
        color: 'black',
    },
    Button: {
        marginTop: 25,
    }
})
export default openingHours