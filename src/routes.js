import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import registerBoss from './pages/registerBoss';
import registerAdress from './pages/registerAddress';
import openingHours from './pages/openingHours';

const Stack = createStackNavigator();

const routes = () => {
    return(
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name='Cadastro' component={openingHours} options={{ headreShow: false }}/>
                <Stack.Screen name='Cadastrar Empresa' component={registerAdress} options={{ headreShow: false }}/>
                <Stack.Screen name='Definir Horarios' component={openingHours} options={{ headreShow: false }}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}
export default routes;